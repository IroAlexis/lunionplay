/*
 * utils.h
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __UTILS__
#define __UTILS__


#include <glib.h>
#include <glib/gstdio.h>
#include <stdio.h>

#if defined(__STDC_VERSION__) && __STDC_VERSION__ < 202311L
#include <stdbool.h>
#endif



void ln_append_env(const char* name, const char* value, const char* separator);

char** ln_build_argv(const char* bin, const char* format, ...);

char* ln_get_basename(const char* path);

char* ln_get_compatdata_path(void);

int ln_get_debug_mode(void);

char* ln_get_dirname_bin(void);

char* ln_get_exec(char* exec);

char* ln_get_kernel(void);

char* ln_get_output_cmd(const char* cmd);

char* ln_get_kernel(void);

char* ln_get_user_cache_path(void);

char* ln_get_user_config_path(void);

char* ln_get_user_data_path(void);

char* ln_get_user_runtime_path(void);

char* ln_get_env(const char* suffix_key);

void ln_prepend_env(const char* name, const char* value, const char* separator);

char* ln_get_hyperlink_file(const char* string);

char* ln_remove_linefeed_to_end_string(char** string);

bool ln_reset_env(const char* key, const char* value);

bool ln_run_process(const char* workdir, char** argv, const bool silent);



#endif
