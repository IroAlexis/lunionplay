/*
 * debug.c
 *
 * Copyright (C) 2024 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "debug.h"

#include "../utils.h"

#include <assert.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_MAX 16384



extern char* program_invocation_short_name;


static const char* _debug_class_names[] = {
	"",      /* LN_DBG_CLASS_INFO    */
	"error", /* LN_DBG_CLASS_ERROR   */
	"warn",  /* LN_DBG_CLASS_WARNING */
	"trace"  /* LN_DBG_CLASS_TRACE   */
};

static _LnDebugClass _dbcl_max = LN_DBG_CLASS_WARNING;


FILE* _get_stream(const _LnDebugClass dbcl)
{
	FILE* stream = stdout;

	if (dbcl == LN_DBG_CLASS_ERROR || dbcl == LN_DBG_CLASS_TRACE)
	{
		stream = stderr;
	}

	return stream;
}


int _print_base(FILE* stream)
{
	assert(stream);

	return fprintf(stream, "%s:", program_invocation_short_name);
}


int _print_channel(FILE* stream, const _LnDebugChannel* dbch)
{
	assert(stream);
	assert(dbch);

	return fprintf(stream, "%s:", dbch->name);
}


int _print_class(FILE* stream, const _LnDebugClass dbcl)
{
	assert(stream);

	return fprintf(stream, "%s:", _debug_class_names[dbcl]);
}


void ln_debug_fprintf(const _LnDebugClass dbcl, const _LnDebugChannel* dbch, const char* format, ...)
{
	if (dbcl <= _dbcl_max)
	{
		char buffer[BUFFER_MAX];
		va_list args;
		FILE* stream = NULL;

		stream = _get_stream(dbcl);

		_print_base(stream);
		_print_class(stream , dbcl);
		if (dbcl != LN_DBG_CLASS_INFO)
		{
			_print_channel(stream, dbch);
		}

		va_start(args, format);
		vsnprintf(buffer, sizeof(buffer), format, args);
		va_end(args);

		fprintf(stream, " %s\n", buffer);

		/* Ensure chronological order when stdout redirected */
		fflush(stream);
	}
}


void ln_debug_set_class_max(const _LnDebugClass dbcl)
{
	_dbcl_max = dbcl;
}
