/*
 * session.c
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "session.h"

#include "utils/debug.h"
#include "utils/memory.h"
#include "prefix.h"
#include "translayer.h"
#include "utils.h"

#include <assert.h>



LN_DEFAULT_DEBUG_CHANNEL(session);


static char* _build_session_state_filename(const char* program_id)
{
	assert(program_id);

	char* xdg_runtime = NULL;
	char* file = NULL;

	xdg_runtime = ln_get_user_runtime_path();
	if (xdg_runtime && g_mkdir_with_parents(xdg_runtime, 0700) == 0)
	{
		file = g_build_filename(xdg_runtime, program_id, NULL);
	}

	ln_free(xdg_runtime);

	return file;
}


static bool _save_session_state(const char* file)
{
	assert(file);

	const char* path = NULL;
	bool status = false;

	/* TODO Do not call getenv for get WINEPREFIX value */
	if ((path = g_getenv("WINEPREFIX")))
	{
		GError* error = NULL;

		status = g_file_set_contents(file, path, (gsize) strlen(path), &error);
		if (! status)
		{
			ERR("%s", error->message);
		}

		if (error)
		{
			g_error_free(error);
			error = NULL;
		}
	}

	if (status)
	{
		TRACE("Saving session state into %s", file);
		g_setenv("LUNION_SESSION_STATE_PATH", file, true);
	}

	return status;
}


void ln_session_save(const char* program_id)
{
	assert(program_id);

	char* file = NULL;

	file = _build_session_state_filename(program_id);
	if (file)
	{
		_save_session_state(file);

		ln_free(file);
	}
}


void ln_session_init(const LnWine* wine, const LnProgram* program, const LnDriver driver)
{
	assert(wine);
	assert(program);

	char* cc_path = NULL;

	if ((cc_path = ln_program_get_cache_path(program)))
	{
		ln_session_save(ln_program_get_id(program));

		ln_wine_init(wine);
		ln_translayer_init(DXVK, cc_path);
		ln_translayer_init(VKD3DPROTON, cc_path);
		ln_driver_init(driver, cc_path);

		ln_free(cc_path);
	}
}


void ln_session_exit(const LnWine* wine)
{
	assert(wine);

	ln_wine_wait_server(wine);

	if (g_remove(g_getenv("LUNION_SESSION_STATE_PATH")) != 0)
	{
		ERR("Cannot remove to session state file: %s");
	}
}


int ln_session_run(const LnWine* wine, const LnProgram* program)
{
	assert(wine);
	assert(program);

	char* cmd = NULL;
	int status = -1;

	if ((cmd = ln_program_get_command(program)))
	{
		char* bin = NULL;
		char* tmp = NULL;

		bin = ln_wine_get_bin_path(wine);
		if ((tmp = g_strdup_printf("%s,%s", bin, cmd)))
		{
			char** argv = NULL;

			if ((argv = g_strsplit(tmp, ",", -1)))
			{
				char* path = NULL;

				if ((path = ln_program_get_dirname(program)))
				{
					TRACE("Using working directory: %s", path);
					status = ln_run_process(path, argv, false);

					ln_free(path);
				}

				g_strfreev(argv);
				argv = NULL;
			}

			ln_free(tmp);
		}

		ln_free(bin);
		ln_free(cmd);
	}

	return  status;
}
