/*
 * arch.h
 *
 * Copyright (C) 2023 IroAlexis <iroalexis@outlook.fr>
 *
 * lunion is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * lunion is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __ARCH__
#define __ARCH__


#include <glib.h>
#include <glib/gstdio.h>



typedef enum
{
	LN_ARCH_NULL       = 0x0,
	LN_ARCH_32BIT_MODE = 0x20,
	LN_ARCH_64BIT_MODE = 0x40
} LnArch;


LnArch ln_arch_get_from_binary_file(char* path);

LnArch ln_arch_get_from_string(char* string);



#endif
