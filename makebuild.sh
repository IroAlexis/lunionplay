#!/usr/bin/env sh


if [ "$DEBUG" = 1 ]
then
	set -ex
else
	set -e
fi

if [ ! -d "$1" ]
then
	echo "usage: $0 /path/to/target/directory [--no-package]"
	exit 1
fi

# Found source directory
SRC_DIR=$(dirname "$(readlink -f "$0")")

# Check if git is clean
if ! git -C "$SRC_DIR" diff --quiet
then
	echo "err: Git directory not clean."
	exit 2
fi

# Init version
LUNION_VERSION=$(git -C "$SRC_DIR" describe --tags | sed 's/\([^-]*-g\)/r\1/;s/-/./g;s/v//g')

DEST_DIR="$(readlink -f "$1")/lunion"

# Control other arguments
shift 1

opt_package=true
opt_release=false

while [ $# -gt 0 ]
do
	case "$1" in
		"--no-package")
			opt_package=false
			;;
		"--release")
			opt_release=true
			;;
	esac
	shift
done

# Create dest directory
GIT_BRANCH="$(git -C "$SRC_DIR" branch --show-current)"
if [ $opt_release = true ]
then
	SUFFIX_DIR="$LUNION_VERSION"
elif [ "$GIT_BRANCH" = "main" ]
then
	SUFFIX_DIR=git
else
	SUFFIX_DIR="$(echo "$GIT_BRANCH" | sed 's/\//-/g')"
fi
DEST_DIR="$DEST_DIR-$SUFFIX_DIR"

if [ -d "$DEST_DIR" ]
then
	echo "Build directory $DEST_DIR already exists."
	rm -rf "${DEST_DIR:?}"/*
else
	mkdir "$DEST_DIR"
fi
LUNION_ARCHIVE_PATH="$DEST_DIR.tar.zst"

# Compile and install lunion
BUILD_DIR=/tmp/lunion.build
if [ ! -d "$BUILD_DIR" ]
then
	meson setup \
		--buildtype "release" \
		--prefix "$DEST_DIR" \
		--bindir "$DEST_DIR" \
		"$BUILD_DIR" \
		"$SRC_DIR"
fi
meson compile -C "$BUILD_DIR"

echo "$LUNION_VERSION" > "$BUILD_DIR"/version

install -v -Dm755 "$BUILD_DIR/src/lunion" "$DEST_DIR"
install -v -Dm644 "$BUILD_DIR/version" "$DEST_DIR"
install -v -Dm644 "$SRC_DIR/LICENSE" "$DEST_DIR"
rm -rf "$BUILD_DIR"

create_package()
{
	tar -C "$DEST_DIR/.." --zstd -cf "$LUNION_ARCHIVE_PATH" "$(basename "$DEST_DIR")"
	rm -rf "$DEST_DIR"
}

if [ $opt_package = true ]
then
	create_package
fi

exit 0